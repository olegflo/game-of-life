# README #

Simple implementation of Conway's Game of Life. It uses simplest algorithm implementation, but it could be easily changed. 

If you want to extend this app with another concrete algorithm you have to implement interface IGameOfLife.