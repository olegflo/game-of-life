package com.gameoflife.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gameoflife.R;
import com.gameoflife.algo.IGameOfLife;

/**
 * @author Oleg Soroka
 * @date 5/22/15
 * <p/>
 */
public class GameFieldAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private IGameOfLife gameOfLife;

    public GameFieldAdapter(Context context, IGameOfLife gameOfLife) {
        this.gameOfLife = gameOfLife;
        inflater = LayoutInflater.from(context);
    }

    private int getMatrixSide() {
        return gameOfLife.getFieldSize();
    }

    @Override
    public int getCount() {
        // For square field. For rectangular should be changed.
        return getMatrixSide() * getMatrixSide();
    }

    @Override
    public Byte getItem(int position) {
        int i = position % getMatrixSide();
        int j = position / getMatrixSide();
        return gameOfLife.getCurrentGeneration()[i][j];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.cell_filled_item_layout, null);
            holder = new ViewHolder();
            holder.ivBackground = (ImageView) view.findViewById(R.id.iv_background);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        byte item = getItem(position);
        int visibility = item == 1 ? View.VISIBLE : View.INVISIBLE;
        holder.ivBackground.setVisibility(visibility);

        return view;
    }

    private class ViewHolder {
        ImageView ivBackground;
    }

}
