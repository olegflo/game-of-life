package com.gameoflife.controller;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.AdapterView;

import com.gameoflife.algo.AlgoProvider;
import com.gameoflife.algo.GameStateUtil;
import com.gameoflife.algo.IGameOfLife;
import com.gameoflife.algo.OnGenerationChangedListener;

/**
 * @author Oleg Soroka
 * @date 5/23/15
 * <p/>
 */
public class GameController {

    // Dimension of square game field matrix
    public static final int FIELD_SIZE = 12;
    public static final int FREEZING_TIME = 500; // Ms

    private GameFieldAdapter gameFieldAdapter;
    private IGameOfLife gameOfLife;
    private Context context;
    private Handler handler;
    private LoopGameTask task;

    /**
     * For game field uses GridView which is actually AdapterView.
     * Could be changed for another exact implementation.
     * AdapterView as basic class was chosen in order to implement click listener
     * for grid cells (see setOnItemClickListener()).
     */
    public GameController(Context context, AdapterView gameFieldView) {
        this.context = context;
        handler = new Handler(Looper.getMainLooper());
        setGameFieldView(gameFieldView);
    }

    public void setGameFieldView(AdapterView gameFieldView) {
        // Keep the same instance of game object
        if (gameOfLife == null) {
            gameOfLife = AlgoProvider.getAlgorithm(FIELD_SIZE);
        }
        gameFieldAdapter = new GameFieldAdapter(context, gameOfLife);
        gameFieldView.setAdapter(gameFieldAdapter);
        gameFieldView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gameOfLife.updateCell(position);
                gameFieldAdapter.notifyDataSetChanged();
            }
        });
    }

    public void start() {
        if (task == null || !task.isRunning()) {
            task = new LoopGameTask();
            task.execute();
        }
    }

    public void stop() {
        if (task != null) {
            task.stop();
        }

    }

    public void next() {
        gameOfLife.nextGeneration();
        gameFieldAdapter.notifyDataSetChanged();
    }

    public void clean() {
        gameOfLife.cleanField();
        gameFieldAdapter.notifyDataSetChanged();
    }


    public void setOnGenerationChangedListener(final OnGenerationChangedListener onGenerationChangedListener) {
        gameOfLife.setOnGenerationChangedListener(new OnGenerationChangedListener() {
            @Override
            public void onGenerationChanged(final int generation) {
                // Run in UI thread
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onGenerationChangedListener.onGenerationChanged(generation);
                    }
                });
            }
        });
    }

    public void saveInstanceState(Bundle outState) {
        GameStateUtil.saveInstanceState(gameOfLife, outState);
    }

    public void restoreInstanceState(Bundle savedInstanceState) {
        GameStateUtil.restoreInstanceState(gameOfLife, savedInstanceState);
    }

    //

    class LoopGameTask extends AsyncTask {

        private boolean isRunning = true;

        public void stop() {
            isRunning = false;
        }

        public boolean isRunning() {
            return isRunning;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            while (isRunning) {
                gameOfLife.nextGeneration();
                try {
                    Thread.sleep(FREEZING_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            gameFieldAdapter.notifyDataSetChanged();
        }

    }

    public boolean isGameRunning() {
        return task != null && task.isRunning();
    }

}