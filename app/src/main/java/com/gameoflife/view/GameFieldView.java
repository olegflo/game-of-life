package com.gameoflife.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

import com.gameoflife.controller.GameController;

/**
 * @author Oleg Soroka
 * @date 5/22/15
 * @time 09:46
 * <p/>
 */
public class GameFieldView extends GridView {

    public GameFieldView(Context context) {
        super(context);
        init();
    }

    public GameFieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setNumColumns(GameController.FIELD_SIZE);
    }

    @Override
    protected void onMeasure(int width, int height) {
        // Keep the whole game field square for both orientations
        int min = width < height ? width : height;
        int max = width < height ? height : width;
        if (min < 0) {
            min = max; // Avoid negative value
        }
        super.onMeasure(min, min);
    }

}
