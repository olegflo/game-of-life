package com.gameoflife.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * @author Oleg Soroka
 * @date 5/22/15
 * @time 13:08
 * <p/>
 */
public class CellLayout extends LinearLayout {

    public CellLayout(Context context) {
        super(context);
    }

    public CellLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CellLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Make cell square
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}