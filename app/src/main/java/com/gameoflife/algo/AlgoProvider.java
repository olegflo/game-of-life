package com.gameoflife.algo;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 */
public class AlgoProvider {

    public static IGameOfLife getAlgorithm() {
        return getAlgorithm(0);
    }

    public static IGameOfLife getAlgorithm(int fieldSize) {
        // Specific algorithm implementation could be changed here
        SimpleGameAlgo simpleGameAlgo = new SimpleGameAlgo(fieldSize);
        return simpleGameAlgo;
    }

}