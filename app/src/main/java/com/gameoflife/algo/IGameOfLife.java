package com.gameoflife.algo;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 * Interface definition for communication with game process.
 */
public interface IGameOfLife {

    /**
     * @return length of game square field matrix' side.
     */
    int getFieldSize();

    int getGenerationIndex();

    void setGenerationIndex(int generationIndex);

    /**
     * @return current square byte matrix with representation of generation
     * which is basically game field state where 0 is empty (dead) cell and 1 is filled (live) cell.
     */
    byte[][] getCurrentGeneration();

    void setCurrentGeneration(byte[][] generation);

    /**
     * @return calculated next generation.
     */
    byte[][] nextGeneration();

    /**
     * Updates cell of current generation with specified position.
     * Switches state to opposite, e.g. if state was 0 it changes for 1.
     *
     * @param position Cell coordinate in flat representation of matrix.
     */
    void updateCell(int position);

    /**
     * Sets all cells to empty state.
     */
    void cleanField();

    /**
     * Sets listener which calls when new generation was calculated.
     *
     * @param onGenerationChangedListener
     */
    void setOnGenerationChangedListener(OnGenerationChangedListener onGenerationChangedListener);

}