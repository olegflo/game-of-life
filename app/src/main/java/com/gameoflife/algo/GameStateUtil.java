package com.gameoflife.algo;

import android.os.Bundle;
import android.util.Log;

/**
 * @author Oleg Soroka
 * @date 5/25/15
 * <p/>
 */
public class GameStateUtil {

    public static final String LOG_TAG = GameStateUtil.class.getSimpleName();

    public static final String EXTRA_GEN_INDEX = "EXTRA_GEN_INDEX";
    public static final String EXTRA_GEN_ARRAY = "EXTRA_GEN_ARRAY";

    public static void saveInstanceState(IGameOfLife gameOfLife, Bundle outState) {
        int genIndex = gameOfLife.getGenerationIndex();
        Log.d(LOG_TAG, "saveInstanceState, genIndex=" + genIndex);
        outState.putInt(EXTRA_GEN_INDEX, genIndex);

        byte[][] gen = gameOfLife.getCurrentGeneration();
        // Array should be flatten to have chance store it via bundle
        byte[] flatGen = convertToFlatArray(gen);
        outState.putByteArray(EXTRA_GEN_ARRAY, flatGen);
    }

    public static void restoreInstanceState(IGameOfLife gameOfLife, Bundle savedInstanceState) {
        Log.d(LOG_TAG, "restoreInstanceState");
        if (savedInstanceState != null) {
            int genIndex = savedInstanceState.getInt(EXTRA_GEN_INDEX);
            Log.d(LOG_TAG, "genIndex=" + genIndex);
            byte[] flatGen = savedInstanceState.getByteArray(EXTRA_GEN_ARRAY);
            if (genIndex != 0 && flatGen != null) {
                gameOfLife.setGenerationIndex(genIndex);
                byte[][] arrMatrix = convertToMatrixArray(flatGen);
                gameOfLife.setCurrentGeneration(arrMatrix);
            }
        }
    }

    public static byte[] convertToFlatArray(byte[][] matrix) {
        int length = matrix.length;
        int flatLength = length * length;
        byte[] flatArr = new byte[flatLength];
        for (int i = 0; i < length; i++) {
            System.arraycopy(matrix[i], 0, flatArr, length * i, length);
        }
        return flatArr;
    }

    public static byte[][] convertToMatrixArray(byte[] arr) {
        int flatLength = arr.length;
        int matrixLength = (int) Math.sqrt(flatLength);

        byte[][] matrix = new byte[matrixLength][matrixLength];

        for (int i = 0; i < matrixLength; i++) {
            System.arraycopy(arr, matrixLength * i, matrix[i], 0, matrixLength);
        }

        return matrix;
    }

}
