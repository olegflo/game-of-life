package com.gameoflife.algo;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 */
public interface OnGenerationChangedListener {

    void onGenerationChanged(int generation);

}
