package com.gameoflife.algo;

import java.util.Arrays;

/**
 * @author Oleg Soroka
 * @date 5/23/15
 * <p/>
 */
public class SimpleGameAlgo implements IGameOfLife {

    public static final int DEFAULT_FIELD_SIZE = 10;
    private static final int MIN_FIELD_SIZE = 1;
    private static final int MAX_FIELD_SIZE = 50;

    public static final int DEAD = 0;
    public static final int LIVE = 1;

    private static final int RULE_BORN = 3;
    private static final int RULE_LIVE_1 = 2;
    private static final int RULE_LIVE_2 = 3;

    private byte[][] gen1; // Current generation
    private byte[][] gen2; // Next calculated generation

    private int generationIndex; // Generation index
    private int fieldSize;

    public SimpleGameAlgo() {
        this(DEFAULT_FIELD_SIZE);
    }

    public SimpleGameAlgo(int fieldSize) {
        if (fieldSize < MIN_FIELD_SIZE || fieldSize > MAX_FIELD_SIZE) {
            fieldSize = DEFAULT_FIELD_SIZE;
        }
        this.fieldSize = fieldSize;
        updateGenerationIndex(1);
        gen1 = new byte[fieldSize][fieldSize];
        gen2 = new byte[fieldSize][fieldSize];
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public int getGenerationIndex() {
        return generationIndex;
    }

    public void setGenerationIndex(int generationIndex) {
        this.generationIndex = generationIndex;
    }


    public byte[][] nextGeneration() {
        buildGeneration();
        updateGenerationIndex(getGenerationIndex() + 1);
        updateGenerationListener();

        byte res[][] = Arrays.copyOf(gen2, fieldSize);
        gen1 = gen2;
        gen2 = new byte[fieldSize][fieldSize];
        return res;
    }

    public byte[][] getCurrentGeneration() {
        return gen1;
    }

    public void cleanField() {
        gen1 = new byte[fieldSize][fieldSize];
        updateGenerationIndex(1);
        updateGenerationListener();
    }

    private void buildGeneration() {
        for (byte i = 0; i < fieldSize; i++) {
            for (byte j = 0; j < fieldSize; j++) {
                processLivingRule(i, j);
            }
        }
    }

    private void processLivingRule(byte i, byte j) {
        boolean isLive = checkCell(i, j, gen1);
        byte count = count(i, j, gen1);
        // If dead cell
        if (!isLive) {
            if (ruleBorn(count)) {
                live(i, j, gen2);
            }
        } else {
            // If live cell
            if (ruleLive(count)) {
                live(i, j, gen2);
            } else {
                kill(i, j, gen2);
            }
        }
    }

    private void live(byte x, byte y, byte[][] arr) {
        arr[x][y] = LIVE;
    }

    private void kill(byte x, byte y, byte[][] arr) {
        arr[x][y] = DEAD;
    }

    // Count Moore neighborhood
    private byte count(int i, int j, byte[][] arr) {
        int count = getCellValue(i - 1, j + 1, arr) +
                getCellValue(i, j + 1, arr) +
                getCellValue(i + 1, j + 1, arr) +
                getCellValue(i + 1, j, arr) +
                getCellValue(i + 1, j - 1, arr) +
                getCellValue(i, j - 1, arr) +
                getCellValue(i - 1, j - 1, arr) +
                getCellValue(i - 1, j, arr);
        return (byte) count;
    }

    private byte getCellValue(int i, int y, byte[][] arr) {
        if ((i >= 0 && i < fieldSize) && (y >= 0 && y < fieldSize)) {
            return arr[i][y];
        } else {
            return DEAD;
        }
    }

    private boolean checkCell(int i, int j, byte[][] arr) {
        return arr[i][j] == 1;
    }

    private boolean ruleBorn(byte count) {
        return count == RULE_BORN;
    }

    private boolean ruleLive(byte count) {
        return count == RULE_LIVE_1 || count == RULE_LIVE_2;
    }

    private void printDebugArray(byte arr[][]) {
        for (int i = 0; i < fieldSize; i++) {
            for (int j = 0; j < fieldSize; j++) {
                char c = (char) (arr[i][j] == DEAD ? 183 : 215);
                System.out.print(c + " ");
            }
            System.out.println();
        }
    }

    public void printGeneration() {
        System.out.println("=== Generation " + generationIndex + " ===");
        printDebugArray(gen1);
    }

    public void updateCell(int position) {
        int i = position % fieldSize;
        int j = position / fieldSize;
        switchCellState(i, j, gen1);
    }

    private void switchCellState(int i, int j, byte[][] arr) {
        if (checkCell(i, j, gen1)) {
            gen1[i][j] = DEAD;
        } else {
            gen1[i][j] = LIVE;
        }
    }

    //

    private OnGenerationChangedListener onGenerationChangedListener;

    public void setOnGenerationChangedListener(OnGenerationChangedListener onGenerationChangedListener) {
        this.onGenerationChangedListener = onGenerationChangedListener;
        updateGenerationListener();
    }

    @Override
    public void setCurrentGeneration(byte[][] generation) {
        if (generation == null) {
            throw new IllegalArgumentException("Generation matrix can't be null");
        }
        if (generation.length != fieldSize) {
            throw new IllegalArgumentException("Wrong array size");
        }
        gen1 = Arrays.copyOf(generation, fieldSize);
    }

    private void updateGenerationIndex(int generationIndex) {
        this.generationIndex = generationIndex;
        updateGenerationListener();
    }

    private void updateGenerationListener() {
        if (onGenerationChangedListener != null) {
            onGenerationChangedListener.onGenerationChanged(generationIndex);
        }
    }

}