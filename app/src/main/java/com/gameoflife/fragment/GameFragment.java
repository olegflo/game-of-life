package com.gameoflife.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.gameoflife.R;
import com.gameoflife.algo.OnGenerationChangedListener;
import com.gameoflife.controller.GameController;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 */
public class GameFragment extends Fragment {

    public static final String LOG_TAG = GameFragment.class.getSimpleName();

    public static final String PREFS_GAME_RUNNING_RESTORE = "PREFS_GAME_RUNNING_RESTORE";
    private SharedPreferences prefs;

    private Button btnStart;
    private TextView tvGeneration;
    private GameController gameController;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment after activity was recreated
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_fragment, container, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        AdapterView gameFieldView = (AdapterView) view.findViewById(R.id.game_field_view);
        btnStart = (Button) view.findViewById(R.id.btn_start);
        Button btnNext = (Button) view.findViewById(R.id.btn_next);
        Button btnClean = (Button) view.findViewById(R.id.btn_clean);
        tvGeneration = (TextView) view.findViewById(R.id.tv_generation);

        if (gameController == null) {
            Log.d(LOG_TAG, "create new instance of game");
            gameController = new GameController(getActivity().getApplicationContext(), gameFieldView);
            gameController.restoreInstanceState(savedInstanceState);
        } else {
            // If instance exists after activity recreation, keep old one,
            // but update game field view with new instance
            gameController.setGameFieldView(gameFieldView);
            Log.d(LOG_TAG, "retain existing instance of game");
        }
        startButtonTitleChecker(); // Update start/stop button state

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startButtonHandler();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameController.isGameRunning()) {
                    stopGame();
                }
                gameController.next();
            }
        });
        gameController.setOnGenerationChangedListener(new OnGenerationChangedListener() {
            @Override
            public void onGenerationChanged(final int generation) {
                tvGeneration.setText(getString(R.string.gen) + generation);
            }
        });
        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameController.isGameRunning()) {
                    startButtonHandler();
                }
                gameController.clean();
            }
        });

        // Run game after screen rotation, but stop it if user minimized and restored app
        boolean restoreRunning = prefs.getBoolean(PREFS_GAME_RUNNING_RESTORE, false);
        if (restoreRunning) {
            startGame();
        }

        return view;
    }

    // Handles start/stop button state
    private void startButtonHandler() {
        if (gameController.isGameRunning()) {
            stopGame();
        } else {
            startGame();
        }
    }

    private void startGame() {
        gameController.start();
        startButtonTitleChecker();
    }

    private void stopGame() {
        gameController.stop();
        startButtonTitleChecker();
    }

    private void startButtonTitleChecker() {
        int btnTitle;
        if (gameController.isGameRunning()) {
            btnTitle = R.string.stop;
        } else {
            btnTitle = R.string.start;
        }
        btnStart.setText(btnTitle);
    }

    private void setGameRestoreFlag(boolean flag) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_GAME_RUNNING_RESTORE, flag).commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (gameController.isGameRunning()) {
            stopGame();
            setGameRestoreFlag(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setGameRestoreFlag(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
        gameController.stop();
        setGameRestoreFlag(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        gameController.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

}