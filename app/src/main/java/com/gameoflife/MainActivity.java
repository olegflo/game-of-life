package com.gameoflife;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.gameoflife.fragment.GameFragment;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        String fragmentTag = GameFragment.class.getSimpleName();

        FragmentManager fm = getFragmentManager();
        // Try to find existing fragment (because of using setRetainInstance() method)
        GameFragment gameFragment = (GameFragment) fm.findFragmentByTag(fragmentTag);

        if (gameFragment == null) {
            // If no existing, create new one
            gameFragment = new GameFragment();
            fm.beginTransaction().add(R.id.root_container, gameFragment, fragmentTag).commit();
        }
    }

}