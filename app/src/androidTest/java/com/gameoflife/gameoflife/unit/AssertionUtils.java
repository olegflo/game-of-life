package com.gameoflife.gameoflife.unit;

import android.test.MoreAsserts;

import static junit.framework.Assert.*;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 * Additional assertion methods.
 */
public class AssertionUtils {

    public static void assertMatrix(byte[][] expected, byte[][] actual) {
        assertNotNull(expected);
        assertNotNull(actual);
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            MoreAsserts.assertEquals(expected[i], actual[i]);
        }
    }

}