package com.gameoflife.gameoflife.unit;

import com.gameoflife.algo.IGameOfLife;
import com.gameoflife.algo.SimpleGameAlgo;

import junit.framework.TestCase;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 */
public class SimpleGameAlgoTest extends TestCase {

    public void testConstructor() {
        IGameOfLife gameOfLife = new SimpleGameAlgo(-1);
        assertEquals(SimpleGameAlgo.DEFAULT_FIELD_SIZE, gameOfLife.getFieldSize());

        gameOfLife = new SimpleGameAlgo(0);
        assertEquals(SimpleGameAlgo.DEFAULT_FIELD_SIZE, gameOfLife.getFieldSize());

        gameOfLife = new SimpleGameAlgo(1);
        assertEquals(1, gameOfLife.getFieldSize());

        gameOfLife = new SimpleGameAlgo(20);
        assertEquals(20, gameOfLife.getFieldSize());

        gameOfLife = new SimpleGameAlgo(50);
        assertEquals(50, gameOfLife.getFieldSize());

        gameOfLife = new SimpleGameAlgo(51);
        assertEquals(SimpleGameAlgo.DEFAULT_FIELD_SIZE, gameOfLife.getFieldSize());
    }

}