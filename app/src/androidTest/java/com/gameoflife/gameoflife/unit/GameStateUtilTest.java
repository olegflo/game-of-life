package com.gameoflife.gameoflife.unit;

import android.test.MoreAsserts;

import com.gameoflife.algo.GameStateUtil;

import junit.framework.TestCase;

/**
 * @author Oleg Soroka
 * @date 5/25/15
 * <p/>
 */
public class GameStateUtilTest extends TestCase {

    public void testConvertToFlatArray() {
        byte[][] arr = new byte[][]{
                {10, 11, 12, 13},
                {20, 21, 22, 23},
                {30, 31, 32, 33},
                {40, 41, 42, 43}
        };

        byte[] expectedArr =
                new byte[]{10, 11, 12, 13, 20, 21, 22, 23, 30, 31, 32, 33, 40, 41, 42, 43};

        byte[] flatArr = GameStateUtil.convertToFlatArray(arr);

        MoreAsserts.assertEquals(expectedArr, flatArr);
    }

    public void testConvertToMatrixArray() {
        byte[] arr =
                new byte[]{10, 11, 12, 13, 20, 21, 22, 23, 30, 31, 32, 33, 40, 41, 42, 43};

        byte[][] expectedArr = new byte[][]{
                {10, 11, 12, 13},
                {20, 21, 22, 23},
                {30, 31, 32, 33},
                {40, 41, 42, 43}
        };

        byte[][] matrix = GameStateUtil.convertToMatrixArray(arr);
        AssertionUtils.assertMatrix(expectedArr, matrix);
    }

}