package com.gameoflife.gameoflife.unit;

import com.gameoflife.algo.AlgoProvider;
import com.gameoflife.algo.IGameOfLife;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleg Soroka
 * @date 5/24/15
 * <p/>
 */
public class IGameOfLifeTest extends TestCase {

    public void testCurrentGenerationPositive() {
        byte[][] gen = new byte[8][8];
        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.setCurrentGeneration(gen);
        assertNotNull(gameOfLife.getCurrentGeneration());
        assertEquals(gen.length, gameOfLife.getCurrentGeneration().length);
    }

    public void testCurrentGenerationNegative() {
        try {
            IGameOfLife gameOfLife = AlgoProvider.getAlgorithm();
            gameOfLife.setCurrentGeneration(null);
            fail(); // Shouldn't reach here
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }

    public void testCurrentGenerationMismatchArrSizeNegative() {
        try {
            byte[][] gen = new byte[6][6];
            IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(5);
            gameOfLife.setCurrentGeneration(gen);
            fail(); // Shouldn't reach here
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }


    // Test for Blinker (http://upload.wikimedia.org/wikipedia/commons/9/95/Game_of_life_blinker.gif)
    public void testFigureBlinker() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0}
        };

        byte[][] expectedGen = new byte[][]{
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0}
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.setCurrentGeneration(gen);
        byte[][] nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen, nextGen);
    }

    // Test for Beacon (http://upload.wikimedia.org/wikipedia/commons/1/1c/Game_of_life_beacon.gif)
    public void testFigureBeacon() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0}
        };

        byte[][] expectedGen = new byte[][]{
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0}
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.setCurrentGeneration(gen);
        byte[][] nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen, nextGen);
    }

    // Test for Glider (http://upload.wikimedia.org/wikipedia/commons/f/f2/Game_of_life_animated_glider.gif)
    public void testFigureGlider() {
        byte[][] gen1 = new byte[][]{
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {1, 1, 1, 0},
                {0, 0, 0, 0}
        };

        byte[][] expectedGen2 = new byte[][]{
                {0, 0, 0, 0},
                {1, 0, 1, 0},
                {0, 1, 1, 0},
                {0, 1, 0, 0}
        };

        byte[][] expectedGen3 = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 1, 0},
                {1, 0, 1, 0},
                {0, 1, 1, 0}
        };

        byte[][] expectedGen4 = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1},
                {0, 1, 1, 0}
        };

        byte[][] expectedGen5 = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1},
                {0, 1, 1, 1}
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen1.length);
        gameOfLife.setCurrentGeneration(gen1);
        byte[][] nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen2, nextGen);
        nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen3, nextGen);
        nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen4, nextGen);
        nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen5, nextGen);
    }

    public void testOtherSimpleFigures() {
        // Block (http://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Game_of_life_block_with_border.svg/99px-Game_of_life_block_with_border.svg.png)
        byte[][] figure1 = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 1, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 0}
        };
        byte[][] expectedFigure1 = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 1, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 0}
        };

        // Boat (http://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Game_of_life_boat.svg/123px-Game_of_life_boat.svg.png)
        byte[][] figure2 = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 1, 0},
                {0, 1, 0, 1},
                {0, 0, 1, 0}
        };
        byte[][] expectedFigure2 = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 1, 0},
                {0, 1, 0, 1},
                {0, 0, 1, 0}
        };

        // Beehive (http://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Game_of_life_beehive.svg/147px-Game_of_life_beehive.svg.png)
        byte[][] figure3 = new byte[][]{
                {0, 1, 1, 0},
                {1, 0, 0, 1},
                {0, 1, 1, 0},
                {0, 0, 0, 0}
        };
        byte[][] expectedFigure3 = new byte[][]{
                {0, 1, 1, 0},
                {1, 0, 0, 1},
                {0, 1, 1, 0},
                {0, 0, 0, 0}
        };

        List<byte[][]> figures = new ArrayList<>();
        figures.add(figure1);
        figures.add(figure2);

        List<byte[][]> expectedFigures = new ArrayList<>();
        expectedFigures.add(expectedFigure1);
        expectedFigures.add(expectedFigure2);

        for (int i = 0; i < figures.size(); i++) {
            byte[][] figure = figures.get(i);
            byte[][] expectedFigure = expectedFigures.get(i);
            IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(figure.length);
            gameOfLife.setCurrentGeneration(figure);
            byte[][] nextGen = gameOfLife.nextGeneration();

            AssertionUtils.assertMatrix(expectedFigure, nextGen);
        }
    }

    public void testGenerationDead() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        byte[][] expectedGen = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.setCurrentGeneration(gen);
        byte[][] nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen, nextGen);
    }

    public void testGenerationDead2() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        byte[][] expectedGen = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.setCurrentGeneration(gen);
        byte[][] nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen, nextGen);
    }

    // Tests generation behavior close to the field border.
    // In normal case (not close to the border) it works infinitely (see Blinker).
    public void testGenerationBorderDead() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 1},
                {0, 0, 0, 1},
                {0, 0, 0, 1},
        };

        byte[][] expectedGen2 = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 1, 1},
                {0, 0, 0, 0},
        };

        byte[][] expectedGen3 = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.setCurrentGeneration(gen);
        byte[][] nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen2, nextGen);
        nextGen = gameOfLife.nextGeneration();
        AssertionUtils.assertMatrix(expectedGen3, nextGen);
    }

    public void testUpdateCell() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        byte[][] expectedGen = new byte[][]{
                {0, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.updateCell(5);
        byte[][] currGen = gameOfLife.getCurrentGeneration();
        AssertionUtils.assertMatrix(expectedGen, currGen);
    }

    public void testCleanField() {
        byte[][] gen = new byte[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1},
        };

        byte[][] expectedGen = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.cleanField();
        byte[][] currGen = gameOfLife.getCurrentGeneration();
        AssertionUtils.assertMatrix(expectedGen, currGen);
    }

    public void testGenerationIndex() {
        byte[][] gen = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
        };

        IGameOfLife gameOfLife = AlgoProvider.getAlgorithm(gen.length);
        gameOfLife.nextGeneration(); // 2
        gameOfLife.nextGeneration(); // 3
        gameOfLife.nextGeneration(); // 4
        assertEquals(4, gameOfLife.getGenerationIndex());
    }

}
