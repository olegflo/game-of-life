package com.gameoflife.gameoflife.instrumentation;

import android.test.ActivityInstrumentationTestCase2;

import com.gameoflife.MainActivity;
import com.gameoflife.R;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.not;

/**
 * @author Oleg Soroka
 * @date 5/25/15
 * <p/>
 */
public class SimpleInstrumentationTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public SimpleInstrumentationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        getActivity();
        super.setUp();
    }

    // Check if all necessary views are visible
    public void testInitialState() {
        onView(withId(R.id.root_container)).check(matches(isDisplayed()));
        onView(withId(R.id.btn_clean)).check(matches(isDisplayed()));
        onView(withId(R.id.btn_next)).check(matches(isDisplayed()));
        onView(withId(R.id.game_field_view)).check(matches(isDisplayed()));
        onView(withId(R.id.tv_generation)).check(matches(isDisplayed()));
    }

    public void testNextGeneration() {
        int[] positions = new int[]{0, 1, 2, 3, 4};
        int[] expectedPositions = new int[]{1, 3};

        for (int pos : positions) {
            onData(anything()).inAdapterView(withId(R.id.game_field_view)).atPosition(pos).perform(click());
        }
        onView(withId(R.id.btn_next)).perform(click()).perform(click());

        for (int pos : expectedPositions) {
            onData(anything()).inAdapterView(withId(R.id.game_field_view))
                    .atPosition(pos).onChildView(withId(R.id.iv_background))
                    .check(matches(isDisplayed()));
        }
    }

    public void testClearButton() {
        int[] positions = new int[]{0, 2, 4, 6};

        // Mark live cells
        for (int pos : positions) {
            onData(anything()).inAdapterView(withId(R.id.game_field_view)).atPosition(pos).perform(click());
        }

        // Check if they are visible
        for (int pos : positions) {
            onData(anything()).inAdapterView(withId(R.id.game_field_view))
                    .atPosition(pos).onChildView(withId(R.id.iv_background))
                    .check(matches(isDisplayed()));
        }

        // Clean
        onView(withId(R.id.btn_clean)).perform(click()).perform(click());

        // Check if cleaned
        for (int pos : positions) {
            onData(anything()).inAdapterView(withId(R.id.game_field_view))
                    .atPosition(pos).onChildView(withId(R.id.iv_background))
                    .check(matches(not(isDisplayed())));
        }
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
